<?php

add_action("in_admin_header","excitedash_header_widget");
function excitedash_header_widget()
{
  $content = get_option("excitedash_banner_head");
  if (empty($content))
    return;
  
  echo '<div id="excitedash_header_widget" class="postbox"><div class="inside">';
  echo apply_filters("the_content",$content);
  echo '</div></div>';
}