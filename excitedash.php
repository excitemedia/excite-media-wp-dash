<?php
/**
 * @package Excite_Dash
 * @version 1.4
/*
Plugin Name: Excite Dash
Plugin URI: https://excitemedia.com.au
Description: Banners and promotions from Excite Media
Author: Excite Media
Version: 1.4
Author URI: http://excitemedia.com.au
License: Proprietary
Text Domain: excitedash
Bitbucket Plugin URI: https://bitbucket.org/excitemedia/excite-media-wp-dash
Bitbucket Branch: master
Requires WP:       3.8
Requires PHP:      5.3
*/


define("EXCITEDASH_VERSION", defined("WP_DEBUG") && WP_DEBUG===true ? time() : "1.1");
define("EXCITEDASH_API_URL", "http://excitemedia.com.au/?excite=%s");

require_once dirname(__FILE__)."/branding.php";
require_once dirname(__FILE__)."/widget-dash.php";
require_once dirname(__FILE__)."/widget-login.php";
require_once dirname(__FILE__)."/widget-header.php";

if (!is_dir(__DIR__.'/../github-updater'))
	require_once dirname(__FILE__)."/github-updater.php";

add_action("login_enqueue_scripts","excitedash_login_styles");
function excitedash_login_styles()
{
  wp_enqueue_style("excitedash-branding", plugins_url("css/branding.css",__FILE__), array(), EXCITEDASH_VERSION);
}

add_action("admin_enqueue_scripts","excitedash_admin_styles");
function excitedash_admin_styles()
{
  wp_enqueue_style("excitedash-widgets", plugins_url("css/widgets.css",__FILE__), array(), EXCITEDASH_VERSION);
}

add_action("wp_login","excitedash_download_content");
function excitedash_download_content()
{
  $get = @file_get_contents( sprintf(EXCITEDASH_API_URL,"all") );
  if (!$get || empty($get))
    return;
    
  $downloaded = @unserialize($get);
  if (!$downloaded || empty($downloaded))
    return;
    
  $ok = array("dash","head","login");  
  
  foreach($ok as $for)   
    update_option("excitedash_banner_$for",""); 
  
  foreach($downloaded as $content)
  {
    foreach($content["for"] as $for) 
    {
      update_option("excitedash_banner_$for",$content["content"]); 
    }
  }
  
  update_option("excitedash_last_updated",time());
}
