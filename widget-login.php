<?php

add_action( 'login_footer', 'excitedash_login_widget' );
function excitedash_login_widget()
{
  $content = get_option("excitedash_banner_login");
  if (empty($content))
    return;
  
  echo '<div id="excitedash_login_widget">';
  echo apply_filters("the_content",$content);
  echo '</div>';
}