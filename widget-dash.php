<?php

add_action( 'wp_dashboard_setup', 'excitedash_add_dashboard_widget' );
function excitedash_add_dashboard_widget()
{
  $content = get_option("excitedash_banner_dash");
  if (empty($content))
    return;
  
  wp_add_dashboard_widget(
    'excitedash_dashboard_widget',
    'Excite Media News',
    'excitedash_dashboard_widget'
  );	
  
  global $wp_meta_boxes;

  $normal_dashboard = $wp_meta_boxes['dashboard']['normal']['core'];

  $excite_widget_backup = array( 'excitedash_dashboard_widget' => $normal_dashboard['excitedash_dashboard_widget'] );
  unset( $normal_dashboard['excitedash_dashboard_widget'] );

  $sorted_dashboard = array_merge( $excite_widget_backup, $normal_dashboard );

  $wp_meta_boxes['dashboard']['normal']['core'] = $sorted_dashboard;
  
}

function excitedash_dashboard_widget()
{
  $content = get_option("excitedash_banner_dash");
  if (empty($content))
    return;
  
  echo apply_filters("the_content",$content);
}